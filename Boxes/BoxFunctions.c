#include <stdio.h>
#include <stdlib.h>
#include "BoxHeader.h"

int Create_Items(Items **Head, double V, int Amount){
    Items *temp = (Items*) malloc(sizeof(Items));
    if(temp != NULL){
        temp -> Count = Amount;
        temp -> Item_V = V;
        temp -> next = *Head;
        *Head = temp;
        return 0;
    }
    else return 1;
}
int Check_Fit(double M, Items **Boxes_Head, double V, int Amount){
    int i, k = -1;
    for( i = 0; i < Amount; i ++){
        if((*Boxes_Head)->Item_V >= M){
            (*Boxes_Head)->Item_V -= M;
            k = 0;
        }
        else if(V >= M){
                k =1;
        }
    }
    return k;
}
int Check(double M[], double V, int amount1, int *ammount){
    int Amount = 1, i, choise;
    Items *Boxes_Head = NULL;
    if(Create_Items(&Boxes_Head, V, Amount) ==1){
        printf("Error allocating memory \n");
    }
    else{
        Items *temp = Boxes_Head;
        for( i = 0; i < amount1; i ++){
            (*ammount) ++;
            printf("%d Tikrinam ar %d daiktas tilps i %d deze \n", *ammount, i + 1, Amount);
            Boxes_Head = temp;
            choise = Check_Fit(M[i], &Boxes_Head, V, Amount);
            switch(choise){
                case -1:
                    (*ammount) ++;
                    return -1;
                break;
                case 1:
                    (*ammount) ++;
                    if(Create_Items(&Boxes_Head, V, Amount) ==1){
                        printf("Error allocating memory \n");
                    }
                    else{
                        printf("%d %d daiktas netilpo i deze, todel sukurta nauja deze \n", *ammount, i + 1);
                        temp = Boxes_Head;
                        Amount ++;
                    }
                break;
                case 0:
                    (*ammount) ++;
                    printf("%d %d daiktas tilpo i deze \n", *ammount, i + 1);
                break;
            }
        }
    }
    Delete_List(&Boxes_Head);
    return Amount;
}
void Delete_List(Items **Boxes_Head){
    Items *Current = (*Boxes_Head);
    Items *next;
    while(Current != NULL){
        next = Current -> next;
        free(Current);
        Current = next;
    }
    (*Boxes_Head) = NULL;
}
