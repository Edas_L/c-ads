#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LENGTH 256

int main(int argc, char *argv[]){

  FILE *input;
  FILE *output;

input = fopen(argv[1], "r");
if(input == NULL){
    printf("Nurodyto duomenu failo nera");
    return 0;
}
output = fopen(argv[2], "w");
if(output == NULL){
    printf("Rezultatu failo negalima sukurti");
    return 0;
}

char buff1[LENGTH];
char buff2[LENGTH];
char *ptr, zodis[LENGTH];
int max;

 while(fgets(buff1, LENGTH, input) != NULL){
    ptr = buff1;
    max = 0;
    while(sscanf(ptr,"%s",zodis) == 1){
        Pertvarkymas(zodis, &max, buff2);
        ptr += strlen(zodis);
    }
    if(buff1[0] != 10){
        fprintf(output, buff2);
        fprintf(output, "\n");
    }
}

fclose(input);
fclose(output);

return 0;
}
