#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Functions.h"

#define LENGTH 256

//9.	Banko skolų dengimas (ADT: eilė, prioritetinė eilė). Bankas išduoda klientui paskolą, pagal
//jos sumą ir paskolos laikotarpį sudaromas grąžinimo grafikas (paskolinta suma grąžinama lygiomis dalimis kas 30 dienų).
//Deja, ne visi klientai sugeba grąžinti paskolą laiku. Atėjus mokėjimo dienai klientas gali turėti tik tiek pinigų,
//kiek reikia einamajai įmokai pagal grafiką dengti, arba kažkiek daugiau pinigų (tokiu atveju jis dengia ir įsiskolinimus,
//jei tokių yra), arba kažkiek mažiau pinigų (tokiu atveju auga skolos). Palūkanos skaičiuojamos už visą tam momentui
//negrąžintą paskolos sumą. Už laiku negrąžintą paskolos dalį imami delspinigiai (delspinigiai neskaičiuojami nuo palūkanų
//ir delspinigių). Galimos 2 skolų dengimo strategijos: 1) dengiamos seniausios skolos, neatsižvelgiant į jų tipą; 2) dengiama
//pagal prioritetus: pagrindinė suma, palūkanos, delspinigiai. Įvertinti šių strategijų  ekonominį poveikį klientui. Visi kiti
//rodikliai, nuo kurių priklauso procesas, įvedami kaip programos parametrai.

int main(int argc, char *argv[])
{
    FILE *input;
    FILE *output;

    input = fopen(argv[1], "r");
    if(input == NULL){
        printf("Nurodyto duomenu failo nera");
        return 0;
    }
    output = fopen(argv[2], "w");
    if(output == NULL){
        printf("Rezultatu failo negalima sukurti");
        return 0;
    }
    Q *Head = NULL;
    PQ *PHead = NULL;
    double par[5], read;
    double pay = 0, pay2;
    int i, was = 1, check = 0, men;
    double income[14], income2[14];
    char trash[LENGTH];
    for( i = 0; i < 4; i ++){
        fscanf(input, "%lf", &read);
        par[i] = read;
        fgets(trash, LENGTH, input);
    }
    if(par[1] < 1){
        fprintf(output, "Netinkamas paskolos grazinimo terminas \n");
        return 0;
    }
    for( i = 1; i < 14; i ++){
        fscanf(input, "%lf", &read);
        income[i] = read;
        income2[i] = read;
        fgets(trash, LENGTH, input);
    }
    double fee = (par[0] + ((par[0] * par[2]) / 100)) / par[1];
    double sum = par[0] / par[1];
    men = par[1] + 1;
    for( i = 0; i <= par[1]; i ++){
        Insert1(&Head, fee, men);
        men --;
    }
    men = par[1] + 1;
    fprintf(output, "I - skolu grazinimo budas(kai grazinamos seniausios skolos) \n");
    for( i = 0; i <= par[1]; i ++){
        Insert2(&PHead, sum, fee-sum, men);
        men --;
    }
    men = 1;
    fprintf(output, "--------------------------------------------------------- \n");
    fprintf(output, "TIME0: \nSukuriamas paskolos grazinimo grafikas \n");
    fprintf(output, "%f- Paskolos suma(Eur), %f- Paskolos grazinimo laikotarpis(Eur)\n", par[0], par[1]);
    fprintf(output, "%f- Menesinis mokestis(Eur) \n", fee);
    while(Head->next != NULL){
        if(Head-> data > 0){
            Pay1(Head, par, &pay, &was, income, output, &check, &men);
        }
        else{
            Head = Head->next;
        }
    }
    fprintf(output," Isviso zmogus sumokejo %f(Eur) \n\n\n", pay);
    pay2 = pay;
    pay = 0;
    fprintf(output, "II - skolu grazinimo budas(kai grazinamos skolos pagal prioritetus) \n");
    fprintf(output, "--------------------------------------------------------- \n");
    fprintf(output, "TIME0: \nSukuriamas paskolos grazinimo grafikas \n");
    fprintf(output, "%f- Paskolos suma(Eur), %f- Paskolos grazinimo laikotarpis(Eur)\n", par[0], par[1]);
    fprintf(output, "%f- Menesinis mokestis(Eur) \n", fee);
    check = 0;
    was = 1;
    while(PHead->next != NULL){
        if(PHead-> data > 0 || PHead->inter > 0 || PHead->dept > 0){
            if(was > 12){
                was = 1;
            }
            Pay2(PHead, par, &pay, &was, output, &check, income2);
        }
        if(PHead->data ==0 && PHead->inter == 0 && PHead->dept ==0){
            PHead = PHead->next;
        }
    }
    fprintf(output,"Isviso zmogus sumokejo %f(Eur) \n", pay);
    if(pay2 >= pay){
        fprintf(output,"Ekonominiu atzvilgiu klientui labiau apsimoka taikyti antraji skolu dengimo buda \n");
    }
    else{
        fprintf(output,"Ekonominiu atzvilgiu klientui labiau apsimoka taikyti pirmaji skolu dengimo buda \n");
    }
    fclose(input);
    fclose(output);

    return 0;
}
