#include <stdlib.h>
#include <stdio.h>
#include "Functions.h"

void Insert1(Q **Head, double fee, int men){
    Q* temp = (Q*)malloc(sizeof(Q));
    temp->data = fee;
    temp->month = men;
    temp->next = *Head;
    *Head = temp;
}
void Pay1(Q *Head, double M[], double *pay, int *was, double M2[], FILE *output, int *check, int month){
    (*check) ++;
    Q *temp1 = Head;
    Q *temp2 = Head;
    temp1 = temp1->next;
    int i, tempo;
    int men = 0, a = 1;
    double left = 0;
    while(Head->next != NULL){
        if((*was) > 12){
            (*was) = 1;        }
        if(a > 8){
            a = 1;
        }
        M[4] = M2[(*was)];
        fprintf(output, "--------------------------------------------------------- \n");
        fprintf(output,"TIME%d: \n", *was);
        fprintf(output, "--------------------------------------------------------- \n");
        fprintf(output, "Zmogus turi - %f(Eur) \n", M[4]);
        if (Head->data <= M[4]){
            fprintf(output,"Zmogus turi pakankamai pinigu, kad sumoketu visa suma - sumoka %f(Eur) \n",M[4] - (M[4] - Head -> data));
            *pay += M[4] - (M[4] - Head -> data);
            left = M[4] - Head -> data;
            fprintf(output, "Zmogui lieka %f(Eur) \n", left);
            Head->data = 0;
            if(left > 0){
                printf(output, "Uz likusius pinigus zmogus apmoka skolas(jei tokiu yra) \n");
            }
            while(temp1->next != NULL && left != 0){
                Delay(&temp1, &left, &pay, output, a);
                a ++;
            }
            if(left >0){
                    fprintf(output, "Zmogus atidave pinigus uz paskola! \n");
                    break;
            }
        }
        else{
            *pay += M[4];
            Head->data -= M[4];
            fprintf(output, "Zmogus neturi pakankamai pinigu, kad sumoketu visa suma - sumoka %f(Eur), lieka sumoketi - %f(Eur)\n", M[4],Head->data);
        }
        tempo = men;
        if(*check > 1){
            men = M[1];
        }
        month = 1;
        Dept(temp2, men, M, output, &month);
        men = tempo;
        men ++;
        (*was) ++;
        Head = Head->next;
    }
}
void Delay(Q **Head, double *left, double **pay, FILE *output, int a){
    if((*Head)->data > 0){
        if(*left >= (*Head)->data){
            fprintf(output, "%d-menesio skola apmoketa, zmogus sumokejo %f(Eur) \n", (*Head)->month, *left - (*left - ((*Head) -> data)));
            **pay += *left - (*left - ((*Head) -> data));
            *left -= (*Head)->data;
            fprintf(output, "Jam dar liko %f(Eur) \n", *left);
            (*Head)->data = 0;
            (*Head) = (*Head)->next;
        }
        else{
            (*Head)->data -= *left;
            fprintf(output, "Neuzteko apmoketi pilnos %d-menesio skolos sumos, zmogus apmokejo %f(Eur), o skolos liko %f(Eur) \n", (*Head)->month, *left, (*Head)->data);
            **pay += *left;
            *left = 0;
        }
    }
    else{
        (*Head) = (*Head)->next;
    }
}
void Dept(Q *Head, int men, double M[], FILE *output, int *month){
    int i = 0;
    while(Head->next != NULL && i <= men){
        if(Head->data > 0){
            Head->data += ((Head->data * M[3])/100);
            fprintf(output, "%d-menesio skola padideja iki %f(Eur) \n", Head->month, Head->data);
        }
        Head = Head->next;
        i ++;
        (*month) ++;
        if(*month > 12){
            *month = 1;
        }
    }
}
