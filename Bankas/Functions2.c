#include <stdlib.h>
#include <stdio.h>
#include "Functions.h"

void Insert2(PQ **Head, double sum, double fee, int men){
    PQ* temp = (PQ*)malloc(sizeof(PQ));
    temp->data = sum;
    temp->inter = fee;
    temp->month = men;
    temp->dept = 0;
    temp->next = *Head;
    *Head = temp;
}
void Pay2(PQ *PHead, double M[], double *pay, int *was, FILE *output, int *check, double M2[]){
    int month = 1;
    (*check) ++;
    PQ *temp1 = PHead;
    PQ *temp2 = PHead;
    PQ *temp3 = PHead;
    temp1 = temp1->next;
    int i, tempo, men = 0, a;
    double left = 0, temp;
    while(PHead->next != NULL){
        if((*was) > 12){
        (*was) = 1;
    }
        a = 1;
        M[4] = M2[(*was)];
        fprintf(output, "--------------------------------------------------------- \n");
        fprintf(output,"TIME%d: \n", *was);
        fprintf(output, "--------------------------------------------------------- \n");
        fprintf(output, "Zmogus turi - %f(Eur) \n", M[4]);
        if ((PHead->data + PHead->inter + PHead->dept) <= M[4]){
            fprintf(output,"Zmogus turi pakankamai pinigu, kad sumoketu visa suma - sumoka %f(Eur) \n",(PHead->data + PHead->inter + PHead->dept));
            *pay += M[4] - (M[4] - (PHead->data + PHead->inter + PHead->dept));
            left = M[4] - (PHead->data + PHead->inter + PHead->dept);
            fprintf(output, "Zmogui lieka %f(Eur) \n", left);
            PHead->data = 0;
            PHead->inter = 0;
            PHead->dept = 0;
            if(left > 0){
                fprintf(output, "Uz likusius pinigus zmogus apmoka skolas(jei tokiu yra - moka pagal prioritetus) \n");
            }
            while(temp1->next != NULL && left != 0){
                Delay2(&temp1, &left, &pay, output, a);
                Sort_By_Priority(temp1);
                a ++;
            }
            if(left >0){
                    fprintf(output, "Zmogus atidave pinigus uz paskola! \n");
                    break;
            }
        }
        else{
            fprintf(output, "Zmogus neturi pakankamai pinigu sumoketi visos sumos(Vyksta mokejimas): \n");
            *pay += M[4];
            if(M[4] >= PHead->data){
                left = M[4] - PHead->data;
                fprintf(output, "Zmogui sumokejus %f(Eur)-vis pagrindine suma, lieka %f(Eur)\n", PHead->data, left);
                PHead->data = 0;
            }
            else{
                left = 0;
                fprintf(output, "Zmogui sumokejus %f(Eur)-pagrindines sumos, nebelieka pinigu. Jam dar lieka moketi - %f(Eur) \n", M[4],PHead->data - M[4]);
                PHead->data -= M[4];
            }
            if(left > 0){
                temp = left;
                if(left >= PHead->inter){
                    temp = left - PHead->inter;
                    fprintf(output, "Zmogui sumokejus %f(Eur)-visas palukanas lieka %f(Eur) \n", PHead->inter, temp);
                    PHead->inter = 0;
                }
                else{
                    left = 0;
                    fprintf(output, "Zmogui sumokejus %f(Eur)-palukanu, nebelieka pinigu. Jam dar lieka moketi - %f(Eur) \n", left, PHead->inter - temp);
                    PHead->inter -= temp;
                    temp = 0;
                }
                if(left > 0){
                    left = temp;
                    if(left >= PHead->dept){
                        temp = left - PHead->dept;
                        PHead->dept = 0;
                    }
                    else{
                        fprintf(output, "Zmogui sumokejus %f(Eur)-delspinigiu, nebelieka pinigu. Jam dar lieka moketi - %f(Eur) \n", left, PHead->dept - temp);
                        left = 0;
                        PHead->dept -= temp;
                        temp = 0;
                    }
                }
            }
        }
        tempo = men;
        if(*check > 1){
            men = M[1];
        }
        month = 1;
        Dept2(temp2, men, M, output, &month);
        men = tempo;
        men ++;
        (*was) ++;
        PHead = PHead->next;
    }
}
void Delay2(PQ **Head, double *left, double **pay, FILE *output, int a){

    if(((*Head)->data + (*Head)->inter + (*Head)->dept) > 0){
        fprintf(output, "%d-menesis:", (*Head)->month);
        if(*left >= (*Head)->data){
            fprintf(output, "Dalis skolos apmoketa, zmogus sumokejo %f(Eur) pagrindines sumos \n", *left - (*left - (*Head) -> data));
            **pay += *left - (*left - (*Head) -> data);
            (*left) -=(*Head) -> data;
            fprintf(output, "Jam dar liko %f(Eur) \n", *left);
            (*Head)->data = 0;
            if(*left >= (*Head)->inter){
                fprintf(output, "Dalis skolos apmoketa, zmogus sumokejo %f(Eur) palukanu \n", *left - (*left - (*Head) -> inter));
                **pay += *left - (*left - (*Head)->inter);
                (*left) -= (*Head)->inter;
                fprintf(output, "Jam dar liko %f(Eur) \n", *left);
                (*Head)->inter = 0;

                if(*left >= (*Head)->dept){
                    fprintf(output, "Skola apmoketa, zmogus sumokejo %f(Eur) delspinigiu\n", *left - (*left - (*Head) -> dept));
                    **pay += *left - (*left - (*Head)->dept);
                    (*left) -= (*Head)->dept;
                    fprintf(output, "Jam dar liko %f(Eur) \n", *left);
                    (*Head)->dept = 0;
                }
            }
        }
        if(*left < (*Head)->data){
            (*Head)->data -= *left;
            fprintf(output, "Pinigu neuzteko apmoketi visai sumai, zmogus sumokejo %f(Eur) pagrindines sumos, jam dar liko sumoketi %f(Eur)\n", *left, (*Head)->data);
            **pay += *left;
            *left = 0;
        }
        if(*left < (*Head)->inter){
            (*Head)->inter -= *left;
            fprintf(output, "Pinigu neuzteko apmoketi visai sumai, zmogus sumokejo %f(Eur) palukanu, jam dar liko sumoketi %f(Eur) \n", *left, (*Head)->inter -= *left);
            **pay += *left;
            *left = 0;
        }
        if(*left < (*Head)->dept){
            (*Head)->dept -= *left;
            fprintf(output, "Pinigu neuzteko apmoketi visai sumai, zmogus sumokejo %f(Eur) delspinigiu, jam dar liko sumoketi %f(Eur) \n", *left, (*Head)->dept -= *left);
            **pay += *left;
            *left = 0;
        }
    }
    else{
        (*Head) = (*Head)->next;
    }
}
void Dept2(PQ *PHead, int men, double M[], FILE *output, int *month){
    int i = 0;
    while(PHead->next != NULL && i <= men){
        if((PHead->data + PHead->inter + PHead->dept) > 0){
            PHead->dept += (((PHead->data + PHead->inter + PHead->dept) * M[3])/100);
            fprintf(output, "%d-menesio skola padideja iki %f(Eur) \n", PHead->month, (PHead->data + PHead->inter + PHead->dept));
        }
        PHead = PHead->next;
        (*month) ++;
        if(*month > 12){
            *month = 1;
        }
        i ++;
    }
}
void Sort_By_Priority(PQ *PHead){
    PQ *temp;
    double a, b, c;
    int d;
    while(PHead->next != NULL){
        temp = PHead;
        temp = temp->next;
        while(temp->next != NULL){
            if(temp->data > PHead->data || (temp->data == PHead->data && temp->inter > PHead->inter) ||(temp->data == PHead->data && temp->inter == PHead->inter && temp->dept > PHead->dept)){
                a = PHead->data;
                b = PHead->inter;
                c = PHead->dept;
                d = PHead->month;
                PHead->data = temp->data;
                PHead->dept = temp->dept;
                PHead->inter = temp->inter;
                PHead->month = temp->month;
                temp->data = a;
                temp->inter = b;
                temp->dept = c;
                temp->month = d;
            }
            temp = temp->next;
        }
        PHead = PHead->next;
    }
}
