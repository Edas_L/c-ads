#ifndef FUNCTIONS_H_INCLUDED
#define FUNCTIONS_H_INCLUDED

typedef struct PriorityQueue{
    double data;
    int month;
    double dept;
    double inter;
    struct PriorityQueue *next;
}PQ;
typedef struct Queue{
    double data;
    int month;
    struct Queue *next;
}Q;


#endif // FUNCTIONS_H_INCLUDED
