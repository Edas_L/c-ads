#include <stdlib.h>
#include <stdio.h>
#include "PriorityQ.h"

struct PrQueueErr * Create(struct PrQueueErr **first, int *ErrorAllocating)
{
    if(*first == NULL)
    {
        *first = (struct PrQueueErr * )malloc(sizeof(struct PrQueueErr));
        if(*first == NULL)
        {
            *ErrorAllocating = 1;
            return NULL;
        }
        (*first) -> exists = 1;
        (*first) -> blank = 1;
        (*first) -> ErrorCreating = 0;
        return *first;
    }
    else
    {
        (*first) -> ErrorCreating = 1;
        return *first;
    }
}
int IsEmpty(struct PrQueueErr **first)
{
    if(*first == NULL)
		return 0;
    else if((*first) -> blank == 1)
		return 1;
    else
        return 2;
}
void Insert(struct PrQueueErr **first, TYPE *givenValue, int *givenPriority, int *ErrorAllocating)
{
    if((*first) -> blank == 1)
    {
        (*first) -> next = (struct PrQueue * )malloc(sizeof(struct PrQueue));
        (*first) -> next -> value = *givenValue;
        (*first) -> next -> priority = *givenPriority;
        (*first) -> blank = 0;
        return;
    }
    struct PrQueue *element;
    element = (struct PrQueue * )malloc(sizeof(struct PrQueue));
    if(element == NULL)
    {
        *ErrorAllocating = 1;
        return;
    }
    element -> value = *givenValue;
    element -> priority = *givenPriority;
    if(element -> priority > (*first) -> next -> priority)
    {
        struct PrQueue *temporary;
        temporary = (*first) -> next;
        (*first) -> next = element;
        (*first) -> next -> next = temporary;
    }
    else if((*first) -> next -> next == NULL && element -> priority <= (*first) -> next -> priority)
        (*first) -> next -> next = element;
    else
    {
        struct PrQueue *temporary;
        temporary = (*first) -> next;
        while(temporary -> next != NULL && element -> priority <= temporary -> next -> priority)
            temporary = temporary -> next;
        if(temporary -> next == NULL && element -> priority <= temporary -> priority)
            temporary -> next = element;
        else if(temporary -> next -> priority <= element -> priority)
        {
            element -> next = temporary -> next;
            temporary -> next = element;
        }
    }
}
void Pop(struct PrQueueErr **first, TYPE *topValue, int *topPriority)
{
    *topValue = (*first) -> next -> value;
    *topPriority = (*first) -> next -> priority;
    if((*first) -> next -> next == NULL)
    {
        free((*first) -> next);
        (*first) -> blank = 1;
    }
    else
    {
        struct PrQueue *temporary;
        temporary = (*first) -> next -> next;
        free((*first) -> next);
        (*first) -> next = temporary;
    }
}
void Delete(struct PrQueueErr **first)
{
    struct PrQueue *temporary, *temporary2;
    if((*first) -> next != NULL)
    {
        temporary = (*first) -> next;
        while(temporary -> next != NULL)
        {
            temporary2 = temporary -> next;
            free(temporary);
            temporary = temporary2;
        }
        free(temporary);
    }
    free(*first);
    *first = NULL;
}
