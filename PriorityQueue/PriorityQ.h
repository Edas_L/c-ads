#ifndef PriorityQ_h
#define PriorityQ_h
#define TYPE int

typedef struct PrQueueErr
{
    int exists;
	int blank;
	int ErrorCreating;
	struct PrQueue *next;
}PrQueueErr;

typedef struct PrQueue
{
	TYPE value;
	int priority;
	struct PrQueue *next;
}PrQueue;

extern struct PrQueueErr * Create(struct PrQueueErr * *, int *);
extern int IsEmpty(struct PrQueueErr * *);
extern void Insert(struct PrQueueErr * *, TYPE * , int *, int *);
extern void Pop(struct PrQueueErr * *, TYPE * , int * );
extern void Delete(struct PrQueueErr * *);

#define malloc malloc_with_metrics
#define free   free_with_metrics
extern size_t num_current_allocs;
extern void* malloc_with_metrics(size_t size);
extern void free_with_metrics(void* ptr);

#endif
