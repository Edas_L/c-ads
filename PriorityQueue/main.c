#include <stdio.h>
#include <stdlib.h>
#include "PriorityQ.h"

#undef malloc
#undef free

size_t num_allocs = 0;

void* malloc_with_metrics(size_t size)
{
    ++num_allocs;
    return malloc(size);
}

void free_with_metrics(void* ptr)
{
    --num_allocs;
    free(ptr);
}

int main()
{
    int givenPriority, topPriority, choice, ErrorAllocating = 0;
    TYPE givenValue, topValue;
    struct PrQueueErr *first = NULL;
    system("cls");

    printf("1 - Sukurti tuscia prioritetine eile");
    printf("\n2 - Nustatyti, ar eile tuscia");
    printf("\n3 - Ideti nauja elementa su prioritetu");
    printf("\n4 - Isimti elementa su didziausiu prioritetu");
    printf("\n5 - Istrinti visa eile");
    printf("\n6 - Baigti darba su programa\n");

    while(1)
    {
        printf("Iveskite pasirinkima:");
        scanf("%d", &choice);
        switch(choice)
        {
            case 1:
                first = Create(&first, &ErrorAllocating);
                if(ErrorAllocating == 1)
                {
                    printf("Nepavyko isskirti atminties.");
                    ErrorAllocating = 0;
                }
                else if (first -> ErrorCreating == 1)
                {
                    printf("Nepavyko sukurti eiles, greiciausiai ji jau sukurta.\n");
                    first -> ErrorCreating = 0;
                }
                else
                    printf("Sukurete tuscia prioritetine eile.\n");
                break;
            case 2:
                if(IsEmpty(&first) == 0)
                {
					printf("Eile nesukurta.\n");
					break;
                }
				if(IsEmpty(&first) == 1)
				{
					printf("Eile tuscia.\n");
					break;
				}
                if(IsEmpty(&first) == 2)
                {
                    printf("Eile ne tuscia.\n");
                    break;
                }
            case 3:
                if(first == NULL || first -> exists != 1)
                {
                    printf("Pirma sukurkite eile!\n");
                    break;
                }
                else
                {
                    printf("\nNurodykite norima iterpti reiksme - ");
                    scanf("%d", &givenValue);
                    printf("\nNurodykite jos prioriteta - ");
                    scanf("%d", &givenPriority);
                    Insert(&first, &givenValue, &givenPriority, &ErrorAllocating);
                    if(ErrorAllocating == 1)
                    {
                        printf("Nepavyko isskirti atminties.");
                        ErrorAllocating = 0;
                    }
                    break;
                }
            case 4:
                if(first == NULL || first -> exists != 1)
                {
                    printf("Pirma sukurkite eile!\n");
                    break;
                }
                else if(first == NULL || first -> blank != 0)
                {
                    printf("Eile tuscia!\n");
                    break;
                }
                else
                {
                    Pop(&first, &topValue, &topPriority);
                    printf("Elemento reiksme - %d, elemento prioritetas - %d\n", topValue, topPriority);
                    break;
                }
            case 5:
                if(first == NULL)
                {
                    printf("Pirma sukurkite eile!\n");
                    break;
                }
                else
                {
                    Delete(&first);
                    printf("Istrynete eile.\n");
                    break;
                }
            case 6:
                printf("size - %d", num_allocs);
                exit(0);
            default:
                    printf("Neteisingas pasirinkimas.\n");
        }
    }
}
