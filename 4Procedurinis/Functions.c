#include <stdio.h>
#include <stdlib.h>
#include "Struktura.h"

void Prideti_Elementa(Sarasas **pradzia, int reiksme)
{
    Sarasas *sekantis = (Sarasas*) malloc (sizeof (Sarasas));
    sekantis->skaicius = reiksme;
    sekantis->kitas = (*pradzia);
    *pradzia = sekantis;
}
void Apvertimas(Sarasas** pradzia)
{
    Sarasas *dabartinis, *pries, *kitas;
    dabartinis = *pradzia;
    pries = NULL;
    while (dabartinis != NULL) {
        kitas = dabartinis -> kitas;
        dabartinis -> kitas = pries;
        pries = dabartinis;
        dabartinis = kitas;
    }
    *pradzia = pries;
}
void Elemento_Pasalinimas(Sarasas *pradzia, int reiksme){
    int i;
    Sarasas *pries = pradzia;
    Sarasas *kitas;
    for (i = 0; i < reiksme; i ++) {
        pries = pries -> kitas;
    }
    kitas = pries -> kitas -> kitas;
    free(pries -> kitas);
    pries -> kitas = kitas;
}


