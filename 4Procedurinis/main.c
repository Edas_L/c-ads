#include <stdio.h>
#include <stdlib.h>
#include "Struktura.h"

void Spausdinti_Sarasa(Sarasas *pradzia);

int main()
{
    Sarasas *pradzia = NULL;
    int reiksme;
    printf ("Iveskite saraso elementus, jeigu norite baigti sarasa, iveskite 0:\n");
    scanf ("%d", &reiksme);
    while (reiksme != 0){
        Prideti_Elementa(&pradzia, reiksme);
        scanf("%d", &reiksme);
    }
    Apvertimas(&pradzia);
    printf("Kuri elementa norite pasalinti?\n");
    scanf("%d", &reiksme);
    Elemento_Pasalinimas(pradzia, reiksme);
    Spausdinti_Sarasa(pradzia);
    if (pradzia != NULL) {
        Spausdinti_Sarasa(pradzia);
        Apvertimas(&pradzia);
        Spausdinti_Sarasa(pradzia);
    }
    else{
        printf("Saraso nera");
    }

    return 0;
}
void Spausdinti_Sarasa(Sarasas *pradzia)
{
    int i = 1;
    printf("Spausdiname sarasa:\n");
    while (pradzia != NULL) {
        printf("%d elementas yra %d\n", i, pradzia->skaicius);
        pradzia = pradzia->kitas;
        i++;
    }
}
