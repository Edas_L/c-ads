#ifndef STRUKTURA_H_INCLUDED
#define STRUKTURA_H_INCLUDED

typedef struct Sarasas_Tag
{
    int skaicius;
    struct Sarasas_Tag *kitas;
}Sarasas;

void Prideti_Elementa(Sarasas **, int);
void Apvertimas(Sarasas **);
void Elemento_Pasalinimas(Sarasas *, int);

#endif
