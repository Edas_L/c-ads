#include <stdio.h>
#include <stdlib.h>

void skaitymas(int *N, int **M);
void tikrinimas(int N, int *M, int *teig, int *neig);

int main()
{
    int N, *M;
    skaitymas(&N, &M);
    int teig[100], neig[100];
    tikrinimas(N, M, teig, neig);
    free(M);

    return 0;
}
void skaitymas(int *N, int **M){
    int i;
    printf("Iveskite sveika skaiciu N: \n");
    scanf("%d", N);
    printf("Iveskite N sveiku skaiciu: \n");
    *M = malloc(*N * sizeof(int));
    for( i = 0; i < N; i ++){
        scanf("%d", &((*M)[i]));
    }
}
void tikrinimas(int N, int *M, int *teig, int *neig){
    int i, tg = 0, ng = 0, p, s = 0;
    for(i = 0; i < N; i ++){
       if(M[i] >= 0){
            teig[tg] = M[i];
            tg ++;
       }
       if(M[i] < 0){
            neig[ng] = M[i];
            ng ++;
       }
    }
    if(tg != N && ng != N){
        if(tg >= ng)p = ng;
        else p = tg;
        for(i = 0; i < p; i ++){
            s += teig[i] * neig[i];
        }
    }
    if(s != 0)printf("Reiskinio - s, reiksme = %d", s);
    else printf("Visi skaiciai vienodo zenklo \n");
}


