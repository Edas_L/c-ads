#ifndef PRIORITEIL_H_INCLUDED
#define PRIORITEIL_H_INCLUDED

#define FORMAT " %d"
typedef int Type;

typedef struct Queues{
    int nr;
    int sk;
    struct PriorityQueue *begin;
    struct Queues *second;
}Node;
typedef struct PriorityQueue{
    Type data;
    int priority;
    struct PriorityQueue *next;
}PQ;

int Create(Node **, int , PQ**);
void Insert(PQ **, Type , int , Node **);
void Print(PQ *, Node *);
void Remove(PQ** , Node **);

#endif // PRIORITEIL_H_INCLUDED
