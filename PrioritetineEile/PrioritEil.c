#include <stdlib.h>
#include <stdio.h>
#include "PrioritEil.h"

int Create(Node **First, int was, PQ**Head)
{
    Node* temp1 = (Node*)malloc(sizeof(Node));
    temp1->nr = was;
    temp1->sk = 0;
    temp1->second = *First;
    *First = temp1;
    PQ* temp2 = (PQ*)malloc(sizeof(PQ));
    temp2->next = *Head;
    temp2->priority = 0;
    *Head = temp2;
    (*First)->begin = *Head;
    if(*First == NULL)return 1;
    else return 0;
}
void Insert(PQ **Head, Type d, int p, Node **First)
{
    PQ* start = (*Head);
    PQ* temp = (PQ*)malloc(sizeof(PQ));
    temp->data = d;
    temp->priority = p;
    temp->next = NULL;

    if ((*Head)->priority <= p) {
        temp->next = *Head;
        (*Head) = temp;
        (*First)->begin = *Head;
    }
    else {
        while (start->next != NULL && start->next->priority > p){
            start = start->next;
        }
        temp->next = start->next;
        start->next = temp;
    }
}
void Print(PQ *Head, Node *First)
{
    int i = 0;
    while(i < First->sk){
        printf("Elementas - %d , prioritetas - %d \n", Head->data, Head->priority);
        Head = Head ->next;
        i ++;
    }
}
void Remove(PQ** Head, Node **First)
{
    PQ* temp = *Head;
    (*Head) = (*Head)->next;
    (*First)->begin = (*Head);
    free(temp);
}


